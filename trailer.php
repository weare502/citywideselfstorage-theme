<?php
/**
 * Template Name: Trailers
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;
$context['trailers'] = Timber::get_posts( array( 'post_type' => 'trailer', 'posts_per_page' => 100 ) );

Timber::render( 'trailers.twig', $context );