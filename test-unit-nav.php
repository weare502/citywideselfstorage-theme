<?php
/**
 * Template Name: Test Unit Nav
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;
$context['home'] = Timber::get_post( get_option('page_on_front') );

Timber::render('test-unit-nav.twig', $context);