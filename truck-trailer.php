<?php
/**
 * Template Name: Truck/Trailer Info
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;

Timber::render('truck-trailer.twig', $context);