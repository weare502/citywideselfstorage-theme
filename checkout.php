<?php
/**
 * Template Name: Checkout
 */


$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;

Timber::render('checkout.twig', $context);