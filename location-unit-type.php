<?php
/**
 * Template Name: Location Unit Type
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;
$context['home'] = Timber::get_post( get_option('page_on_front') );
$context['location'] =  $post->get_field('location')->post_title;

Timber::render('location-unit-type.twig', $context);