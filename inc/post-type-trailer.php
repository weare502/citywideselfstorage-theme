<?php

$labels = array(
	'name'               => __( 'Trailers', 'spha' ),
	'singular_name'      => __( 'Trailer', 'spha' ),
	'add_new'            => _x( 'Add New Trailer', 'spha', 'spha' ),
	'add_new_item'       => __( 'Add New Trailer', 'spha' ),
	'edit_item'          => __( 'Edit Trailer', 'spha' ),
	'new_item'           => __( 'New Trailer', 'spha' ),
	'view_item'          => __( 'View Trailer', 'spha' ),
	'search_items'       => __( 'Search Trailers', 'spha' ),
	'not_found'          => __( 'No Trailers found', 'spha' ),
	'not_found_in_trash' => __( 'No Trailers found in Trash', 'spha' ),
	'parent_item_colon'  => __( 'Parent Trailer:', 'spha' ),
	'menu_name'          => __( 'Trailers', 'spha' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-cart',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title',
	),
);

register_post_type( 'trailer', $args );