<?php

$labels = array(
	'name'               => __( 'Moving Supplies', 'spha' ),
	'singular_name'      => __( 'Moving Supplies', 'spha' ),
	'add_new'            => _x( 'Add New Moving Supplies', 'spha', 'spha' ),
	'add_new_item'       => __( 'Add New Moving Supplies', 'spha' ),
	'edit_item'          => __( 'Edit Moving Supplies', 'spha' ),
	'new_item'           => __( 'New Moving Supplies', 'spha' ),
	'view_item'          => __( 'View Moving Supplies', 'spha' ),
	'search_items'       => __( 'Search Moving Supplies', 'spha' ),
	'not_found'          => __( 'No Moving Supplies found', 'spha' ),
	'not_found_in_trash' => __( 'No Moving Supplies found in Trash', 'spha' ),
	'parent_item_colon'  => __( 'Parent Moving Supplie:', 'spha' ),
	'menu_name'          => __( 'Moving Supplies', 'spha' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-products',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title', 'thumbnail'
	),
);

register_post_type( 'supplies', $args );