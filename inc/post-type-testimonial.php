<?php

$labels = array(
	'name'               => __( 'Testimonials', 'spha' ),
	'singular_name'      => __( 'Testimonial', 'spha' ),
	'add_new'            => _x( 'Add New Testimonial', 'spha', 'spha' ),
	'add_new_item'       => __( 'Add New Testimonial', 'spha' ),
	'edit_item'          => __( 'Edit Testimonial', 'spha' ),
	'new_item'           => __( 'New Testimonial', 'spha' ),
	'view_item'          => __( 'View Testimonial', 'spha' ),
	'search_items'       => __( 'Search Testimonials', 'spha' ),
	'not_found'          => __( 'No Testimonials found', 'spha' ),
	'not_found_in_trash' => __( 'No Testimonials found in Trash', 'spha' ),
	'parent_item_colon'  => __( 'Parent Testimonial:', 'spha' ),
	'menu_name'          => __( 'Testimonials', 'spha' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-star-half',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title',
	),
);

register_post_type( 'testimonial', $args );