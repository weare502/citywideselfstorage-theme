<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

Timber::$dirname = array('templates', 'views');

class CitywideSelfStorageSite extends TimberSite {

	function __construct() {
		// add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'title-tag' );

		add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'admin_head', array( $this, 'admin_head_css' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );

		add_action( 'init', array( $this, 'register_shortcake') );
		add_action( 'init', array( $this, 'add_acf_options_page' ) );
		
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'mce_buttons_2', array( $this, 'tiny_mce_buttons' ) );
		add_filter( 'tiny_mce_before_init', array( $this, 'tiny_mce_insert_formats' ) );
		add_filter( 'dashboard_glance_items', array( $this, 'dashboard_glance_items' ) );
		add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

		add_filter( 'acf/load_field/name=footer_call_to_action', array( $this, 'footer_cta_items' ) );

		add_action( 'init', function(){
			add_editor_style('style.css' );
		} );

		add_action('widgets_init', function(){
			register_sidebar( array( 'name' => 'Blog Sidebar', 'id' => 'primary' ) );
		});

		parent::__construct();
	}

	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['logo'] = trailingslashit( get_template_directory_uri() ) . 'static/images/logo.png';
		$context['year'] = date('Y');
		$context['options'] = get_fields('option');
		$context['unit_matrix'] = $context['options']['unit_matrix'];
		$context['is_home'] = is_home();
		$context['csscache'] = filemtime(get_stylesheet_directory() . '/style.css');
		$context['plugin_content'] = TimberHelper::ob_function( 'the_content' );
		
		return $context;
	}

	function after_setup_theme(){
		register_nav_menu( 'primary', 'Main Navigation' );
		register_nav_menu( 'footer', 'Footer Resources' );
		// Images Sizes
		add_image_size( 'xlarge', 2880, 2000 );
	}

	function enqueue_scripts(){
		// Dependencies
		wp_enqueue_style( 'magnific-popup-style', get_template_directory_uri() . '/bower_components/magnific-popup/dist/magnific-popup.css', array(), '20120206' );
		wp_enqueue_style( 'cwss-slick-css', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick.css', '20120206' );
		wp_enqueue_style( 'cwss-slick-theme-css', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick-theme.css', '20120206' );
		
		wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/bower_components/magnific-popup/dist/jquery.magnific-popup.min.js', array( 'jquery' ), '20120206', true );
		wp_enqueue_script( 'cwss-slick', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick.js', array( 'jquery' ), '20120206', true );
		wp_enqueue_script( 'cwss-theme', get_template_directory_uri() . "/static/js/site.js", array( 'jquery', 'underscore', 'magnific-popup' ), '20160823' );
		// wp_enqueue_script( 'cwss-video', get_template_directory_uri() . "/static/js/container.player.min.js", array( 'jquery', 'underscore' ), '20160820', true );
		wp_enqueue_script( 'cwss-vue', get_template_directory_uri() . "/static/js/vue.js", array( 'jquery', 'underscore' ), '20160822', true );
		wp_enqueue_script( 'cwss-site-vue', get_template_directory_uri() . "/static/js/dist/main.js", array( 'jquery', 'underscore', 'cwss-vue' ), '20160829', true );
		
		wp_localize_script( 'magnific-popup', "wpThemeUrl", get_stylesheet_directory_uri() );

		wp_localize_script( 'cwss-site-vue', 'cwssUnitMatrix', $this->get_unit_matrix() );
		wp_localize_script( 'cwss-site-vue', 'cwssMovingSupplies', $this->get_moving_supplies() );
		wp_localize_script( 'cwss-site-vue', 'cwssTrailers', $this->get_trailers() );
		wp_localize_script( 'cwss-site-vue', 'cwssLocationUnitTypePages', $this->get_location_unit_type_pages() );
		wp_localize_script( 'cwss-site-vue', 'cwssGfAPI', wp_create_nonce( 'gf_api' ) );
	}

	function admin_head_css(){
		?><style type="text/css">
			.mce-ico.fa { font-family: 'FontAwesome', 'Dashicons'; }
		</style><?php
	}

	function tiny_mce_buttons( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}

	// Callback function to filter the MCE settings
	function tiny_mce_insert_formats( $init_array ) {  
		// Define the style_formats array
		$style_formats = array(  
			// Each array child is a format with it's own settings
			array(  
				'title'    => 'Button',  
				'selector' => 'a',  
				'classes'  => 'button',
				// font awesome must be available in the admin area to see the icon
				'icon'     => ' fa fa-hand-pointer-o'
			),
			// array(  
			// 	'title'    => 'Gold Text',  
			// 	// 'selector' => '*',
			// 	'inline' => 'span',
			// 	'classes'  => 'gold-text',
			// 	// font awesome must be available in the admin area to see the icon
			// 	'icon'     => ' fa fa-eye-dropper'
			// ),
		);  
		// Insert the array, JSON ENCODED, into 'style_formats'
		$init_array['style_formats'] = json_encode( $style_formats );
		$init_array['body_class'] .= " content ";
		return $init_array;  
	  
	}

	function register_post_types(){
		include_once('inc/post-type-testimonial.php');
		include_once('inc/post-type-location.php');
		include_once('inc/post-type-trailer.php');
		include_once('inc/post-type-supplies.php');
	}

	function register_shortcake(){
		include 'inc/shortcake.php';
	}

	function dashboard_glance_items( $items ){
		foreach ( get_post_types(array('public'=>true)) as $post_type ){
			$num_posts = wp_count_posts( $post_type );
			if ( $num_posts && $num_posts->publish ) {
				if ( 'post' == $post_type ) {
					continue;
				}
				if ( 'page' == $post_type ) {
					continue;
				}
				$post_type_object = get_post_type_object( $post_type );
				$text = _n( '%s ' . $post_type_object->labels->singular_name, '%s ' . $post_type_object->label, $num_posts->publish );
				$text = sprintf( $text, number_format_i18n( $num_posts->publish ) );
				if ( $post_type_object && current_user_can( $post_type_object->cap->edit_posts ) ) {
					$items[] = sprintf( '<a href="edit.php?post_type=%1$s">%2$s</a>', $post_type, $text );
				} else {
					$items[] = sprintf( '<span>%2$s</span>', $post_type, $text );
				}
			}
		}
		
		return $items;
	}

	function add_acf_options_page(){
		if ( ! function_exists('acf_add_options_page') ){
			return;
		}

		acf_add_options_page(array(
			'page_title' 	=> 'Site Options',
			'menu_title'	=> 'Site Options',
			'menu_slug' 	=> 'citywide-site-options',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));

		acf_add_options_page(array(
			'page_title' 	=> 'Unit Matrix',
			'menu_title'	=> 'Unit Matrix',
			'menu_slug' 	=> 'citywide-unit-matrix',
			'capability'	=> 'edit_posts',
			'redirect'		=> false,
			'menu_icon'		=> 'dashicons-grid-view'
		));

	}

	function get_unit_matrix(){
		$matrix = get_field('unit_matrix', 'option');
		
		
		foreach ( $matrix as $key => $unit ){
			// var_dump($unit);
			if ( is_int( $matrix[$key]['location'] ) ){
				$matrix[$key]['location'] = Timber::get_post($matrix[$key]['location']);
			}
			$features = $matrix[$key]['location']->get_field('popup_features');
			
			// $features_filtered = array();
			
			// foreach ( $features as $feature ){
			// 	$features_filtered[] = $feature['name'];
			// }
			
			$matrix[$key]['location']->popup_features = $features;
		}
		
		return $matrix;
	}

	function get_moving_supplies(){
		$supplies = Timber::get_posts( array( 'post_type' => 'supplies', 'posts_per_page' => 100 ) );
		
		foreach ( $supplies as $key => $item ){
			$supplies[$key]->image = $item->get_field('image');
			$supplies[$key]->features = $item->get_field('features');
		}

		return $supplies;
	}
	
	function get_trailers(){
		$trailers = Timber::get_posts( array( 'post_type' => 'trailer', 'posts_per_page' => 100 ) );
		
		foreach ( $trailers as $key => $item ){
			$trailers[$key]->image = $item->get_field('image');
		}

		return $trailers;
	}

	function get_location_unit_type_pages(){
		$pages = Timber::get_posts( array(
			"post_type" => "page",
			"posts_per_page" => 100,
			"meta_query" => array(
				array(
					"key" => 'unit_type'
				),
				array(
					"key" => 'location'
				)
			)
		) );
		foreach ( $pages as $key => $page ){
			$pages[$key]->unit_type = $page->get_field('unit_type');
			$pages[$key]->location = $page->get_field('location')->title;
		}

		return $pages;
	}

}

new CitywideSelfStorageSite();

function citywide_render_primary_menu(){ // used in base.twig
	// Twig won't let us call functions with lots of arguments. This is easier!
	// This render method also works correctly with the customizer selective refresh
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'container' => '',
		'menu_class' => '',
		'menu_id' => 'primary-menu',
	) );
}

function citywide_render_footer_menu(){ // used in base.twig
	// Twig won't let us call functions with lots of arguments. This is easier!
	// This render method also works correctly with the customizer selective refresh
	wp_nav_menu( array(
		'theme_location' => 'footer',
		'container' => '',
		'menu_class' => '',
		'menu_id' => 'footer-menu',
	) );
}

add_action('admin_head', function(){
?>
<style>
	.citywide-acf-matrix-image img {
		max-width: 3rem !important;
	}
</style>
<?php
});