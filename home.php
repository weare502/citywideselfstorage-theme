<?php
/**
 * Blog Template File
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['posts'] = Timber::get_posts();
$context['sidebar'] = Timber::get_widgets('primary');
$context['thumbnail'] = Timber::get_post( get_option('page_for_posts') )->thumbnail();
$templates = array( 'blog.twig' );

Timber::render( $templates, $context );