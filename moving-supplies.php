<?php
/**
 * Template Name: Moving Supplies
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;
$context['moving_supplies'] = Timber::get_posts( array( 'post_type' => 'supplies', 'posts_per_page' => 100 ) );

Timber::render('moving-supplies.twig', $context);