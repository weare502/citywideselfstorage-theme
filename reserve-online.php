<?php
/**
 * Template Name: Reserve Online
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;
$context['locations'] = Timber::get_posts( array( 'post_type' => 'location', 'posts_per_page' => 100, 'order' => 'ASC' ) );

Timber::render('reserve-online.twig', $context);